package br.ucsal.bes20211.testedequalidade.locadora.builder;

import br.ucsal.bes20211.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20211.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20211.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String DEFAULT_PLACA = "ABCD123";
	private static final Integer DEFAULT_ANOFABRICACAO = 2000;
	private static final Double DEFAULT_VALORDIARIA = 50.0;
	private static final SituacaoVeiculoEnum DEFAULT_SITUACAOVEICULO = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa = DEFAULT_PLACA;
	private Integer anoFabricacao = DEFAULT_ANOFABRICACAO;
	private Double valorDiaria = DEFAULT_VALORDIARIA;
	private Modelo modelo;
	private SituacaoVeiculoEnum situacaoVeiculo = DEFAULT_SITUACAOVEICULO;

	private VeiculoBuilder() {

	}

	public static VeiculoBuilder aVeiculo() {
		return new VeiculoBuilder();
	}

	public VeiculoBuilder withPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder withAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
		return this;
	}

	public VeiculoBuilder withValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder withSituacao(SituacaoVeiculoEnum situacaoVeiculo) {
		this.situacaoVeiculo = situacaoVeiculo;
		return this;
	}

	public VeiculoBuilder withModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacaoVeiculo = SituacaoVeiculoEnum.DISPONIVEL;
		return this;

	}

	public VeiculoBuilder locado() {
		this.situacaoVeiculo = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder emManutencao() {
		this.situacaoVeiculo = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder but() {
		return VeiculoBuilder.aVeiculo().withPlaca(placa).withAnoFabricacao(anoFabricacao).withSituacao(situacaoVeiculo)
				.withValorDiaria(valorDiaria).withModelo(modelo);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setModelo(modelo);
		veiculo.setSituacao(situacaoVeiculo);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;

	}
}
