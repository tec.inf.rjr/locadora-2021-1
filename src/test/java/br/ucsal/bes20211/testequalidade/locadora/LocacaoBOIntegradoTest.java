package br.ucsal.bes20211.testequalidade.locadora;

import java.time.LocalDate; 
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20211.testedequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20211.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20211.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20211.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20211.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20211.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes INTEGRADOS para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOIntegradoTest {

	private LocacaoBO locacaoBO;
	private VeiculoDAO veiculoDAO;

	@BeforeEach
	void setup() {

		veiculoDAO = new VeiculoDAO();
		locacaoBO = new LocacaoBO(veiculoDAO);

	}

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano
	 * de fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
		LocalDate dataAtual = LocalDate.now();
		Integer anoAtual = dataAtual.getYear();
		Double valorEsperado = 300.0; //300.0
		Double valorEsperado2 = 405.0;
		Double valorRecebido = 0.0;
		
		Veiculo veiculo1 = VeiculoBuilder.aVeiculo().withAnoFabricacao(anoAtual - 1).withPlaca("ABC1").build();
		Veiculo veiculo2 = VeiculoBuilder.aVeiculo().withAnoFabricacao(anoAtual - 1).withPlaca("ABC2").build();
		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
		List<String> placas = new ArrayList<>();
		placas.add(veiculo1.getPlaca());
		placas.add(veiculo2.getPlaca());

		try {
			valorRecebido = locacaoBO.calcularValorTotalLocacao(placas, 3, dataAtual);
		} catch (VeiculoNaoEncontradoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assertions.assertEquals(valorEsperado, valorRecebido);
		
		placas.clear();
		Veiculo veiculo3 = VeiculoBuilder.aVeiculo().withAnoFabricacao(anoAtual - 8).withPlaca("ABC3").build();
		Veiculo veiculo4 = VeiculoBuilder.aVeiculo().withAnoFabricacao(anoAtual - 8).withPlaca("ABC4").build();
		Veiculo veiculo5 = VeiculoBuilder.aVeiculo().withAnoFabricacao(anoAtual - 8).withPlaca("ABC5").build();
		veiculoDAO.insert(veiculo3);
		veiculoDAO.insert(veiculo4);
		veiculoDAO.insert(veiculo5);
		
		placas.add(veiculo3.getPlaca());
		placas.add(veiculo4.getPlaca());
		placas.add(veiculo5.getPlaca());
		
		try {
			valorRecebido = locacaoBO.calcularValorTotalLocacao(placas, 3, dataAtual);
		} catch (VeiculoNaoEncontradoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assertions.assertEquals(valorEsperado2, valorRecebido);
	}

}
